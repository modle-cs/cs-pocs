using System;
using System.Collections;

namespace ConsoleApplication1 {
    class Program {
        static void Main(string[] args) {
            string text = System.IO.File.ReadAllText(@"./weapons.txt");
            Console.WriteLine("Contents of weapons.txt = {0}", text);

            string[] lines = System.IO.File.ReadAllLines(@"./weapons.txt");
            Console.WriteLine("Contents of weapons.txt = ");
            foreach (string line in lines) {
                Console.WriteLine("\t" + line);
            }

            Random rnd = new Random();
            for (int i = 0; i < lines.Length; i++) {
                string randomWeapon = lines[rnd.Next(lines.Length)];
                Console.WriteLine("Randomized selection from weapons.txt = {0}", randomWeapon);
            }
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}

