using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> myMap = new Dictionary<string, string>();
            myMap["foo"] = "bar";
            myMap["baz"] = "qux";
            myMap["quux"] = "corge";
            myMap["grault"] = "garply";
            myMap["waldo"] = "fred";
            myMap["plugh"] = "xyzzy";
            foreach (KeyValuePair<string, string> kvp in myMap) {
                Console.WriteLine(string.Format("Key = {0}, Value = {1}", kvp.Key, kvp.Value));
            }
            Console.ReadKey(true);
        }
    }
}
