using System;
using System.Collections;

namespace ConsoleApplication1 {
    class Program {
        static void Main(string[] args) {
            ArrayList myAL = new ArrayList();
            myAL.Add("string1");
            myAL.Add("string2");
            myAL.Add("string3");
            myAL.Add("string4");
            myAL.Add("string5");
            myAL.Add("string6");
            foreach ( Object obj in myAL ) {
                Console.WriteLine("Value = {0}", obj);
            }
            Random rnd = new Random();
            for (int i = 0; i < myAL.Count; i++) {
                Object randomValue = myAL[rnd.Next(myAL.Count)];
                Console.WriteLine("Randomized selection from myAL = {0}", randomValue);
            }
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
